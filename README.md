# Sprelf.Singleton

Provides a single abstract class **Singleton**, which makes it simple to create a MonoBehaviour that there can never be any more than one of, and persists through scene transitions.

## Dependencies

None.

## Usage

### Subclassing

```
public class SomeUniqueBehaviour : Singleton<SomeUniqueBehaviour> { ... }
```

### Accessing

```
SomeUniqueBehaviour.Instance.SomeMethod()
```