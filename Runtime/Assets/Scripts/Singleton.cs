using System;
using UnityEngine;

namespace Sprelf.Singleton
{
    /// <summary>
    /// Creates a GameObject in the scene that there can be only one of, and will persist through scene transitions.  Essentially operates
    /// as a static class that has access to <see cref="MonoBehaviour"/> methods, such as <see cref="MonoBehaviour.Update"/>.
    /// <br/>
    /// Usage:
    /// 
    /// <code>
    /// public class SomeBehaviour : Singleton&lt;SomeBehaviour&gt; { ... }
    ///
    /// SomeBehaviour.Instance.SomeMethod();
    /// </code>
    /// </summary>
    /// <typeparam name="T">The <see cref="MonoBehaviour"/> subclass to treat as a Singleton.</typeparam>
    public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T _instance;

        private static readonly object _lock = new object();

        public static bool Quitting { get; private set; }

        public static T Instance
        {
            get
            {
                if (Quitting)
                {
                    throw new SingletonPendingDestructionException<T>();
                }

                lock (_lock)
                {
                    if (_instance != null)
                        return _instance;

                    var instances = FindObjectsOfType<T>();
                    if (instances.Length > 0)
                    {
                        if (instances.Length == 1)
                            return _instance = instances[0];

                        Debug.LogWarning($"[Singleton<{typeof(T)}>] There should never be more than one Singleton of type {typeof(T)} in the scene, but {instances.Length} were found." +
                                         $" The first instance found will be used, and all others will be destroyed.");
                        for (var i = 1; i < instances.Length; i++)
                            Destroy(instances[i]);
                        return _instance = instances[0];
                    }

                    return _instance = new GameObject($"Singleton<{typeof(T)}>").AddComponent<T>();
                }
            }
        }

        public static T I => Instance;

        protected abstract bool _destroyOnLoad { get; }

        private void Awake()
        {
            if (!_destroyOnLoad)
                DontDestroyOnLoad(gameObject);
            lock (_lock)
            {
                if (_instance == null)
                    _instance = (T)this;
            }

            OnAwake();
        }

        /// <summary>
        /// Extra functionality to perform during Awake() step, to be overloaded by subclasses as necessary.
        /// </summary>
        protected virtual void OnAwake()
        {
        }

        private void OnApplicationQuit()
        {
            Quitting = true;
            OnQuit();
        }

        /// <summary>
        /// Extra functionality to perform during OnApplicationQuit() step, to be overloaded by subclasses as necessary.
        /// </summary>
        protected virtual void OnQuit()
        {
        }
    }

    public class SingletonPendingDestructionException<T> : Exception
        where T: Singleton<T>
    {
        public SingletonPendingDestructionException()  
            : base($"[Singleton<{typeof(T)}>] Instance will not be returned because the application is quitting.")
        {
        }
    }
}